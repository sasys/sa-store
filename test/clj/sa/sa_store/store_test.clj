(ns sa.sa-store.store-test
  "
    Test cases for the jvm based store operations
  "
  (:require [clojure.test                :refer [is deftest]]
            [sa.sa-store.store           :refer [summarise-all get-model list-model-names update-model delete-model update-summary delete-all]]
            [sa.sa-store.store-factory   :refer [get-store]]
            [sa.sa-store.couch-test]
;            [pjstadig.humane-test-output :as pj]
            [clojure.string              :refer [starts-with? split]]
            [clojure.pprint              :as prt]))

;For some reason this is not working in my .profiles.clj
;(pj/activate!)

(def memory-store (get-store :sa.sa-store.store-factory/memory-store))
(def file-store (get-store :sa.sa-store.store-factory/file-store))
(def couch-store (get-store :sa.sa-store.store-factory/couch-store))

(def model-1 {:sadf/meta  {:sadf/name "test-model-1"
                           :sadf/uuid "123"
                           :sadf/summary "This is a test model with id 123"
                           :sadf/detail {:sadf/ownership "Owened by dept q"
                                         :sadf/contact   "Fred Smith"
                                         :sadf/contact-email "a@b.com"}}
              :sadf/model {}
              :layout {(keyword "sadf" "1.2") [521 128],}})

(def model-2 {:sadf/meta {:sadf/name "test-model-2"
                          :sadf/uuid "124"
                          :sadf/summary "This is a test model with id 124"
                          :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note"
                                        :sadf/contact   "Not yet assigned"
                                        :sadf/contact-email "z@b.com"}}
              :sadf/model {}})

;This one has position information in it.
(def model-3 {:sadf/meta {:sadf/name "test-model-3"
                          :sadf/uuid "125"
                          :sadf/summary "This is a test model with id 125"
                          :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note"
                                        :sadf/contact   "Not yet assigned"
                                        :sadf/contact-email "z@b.com"}}
              :sadf/procs {}
              :sadf/layout {(keyword "sadf" "0") [450 450]}
              :sadf/flows  [{:sadf/id "2aa7be96-e790-4b95-8943-f5ca548c01b5",
                             :sadf/data [{:sadf/term {:label "pre-production-meta-data", :id "38dfa550-9be8-4a8e-8ee9-1a8b65e39380"}, :sadf/term-id "38dfa550-9be8-4a8e-8ee9-1a8b65e39380"}],
                             :sadf/source [{:sadf/posn [139 501], :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c", :sadf/proc-id :sadf/context}],
                             :sadf/sink [{:sadf/posn [319 455], :sadf/id "4604dfc7-e1f8-4799-88a3-b8023ef83258", :sadf/proc-id :sadf/context}],
                             :sadf/control-point [{:sadf/posn [279 518], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil} {:sadf/posn [279 518], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil}],
                             :sadf/description "Description of flow."}]})

(deftest mem-store
    (is (= '() (list-model-names memory-store)))
    (is (= :not-found (get-model memory-store "test-model-1")))
    (is (= {:123 model-1} (update-model memory-store "123" model-1)))
    (is (= "123" (get-in (get-model memory-store "123") [:sadf/meta :sadf/uuid])))
    (is (= true (delete-model memory-store "123")))
    (is (= '() (list-model-names memory-store)))
    (is (= {:123 model-1} (update-model memory-store "123" model-1)))
    (is (= {:124 model-2} (update-model memory-store "124" model-2)))
    (is (some #{"123"} (list-model-names memory-store)))
    (is (some #{"124"} (list-model-names memory-store)))
    (is (some #{{:sadf/name "test-model-1",
                 :sadf/uuid "123",
                 :sadf/summary "This is a test model with id 123",
                 :sadf/detail {:sadf/ownership "Owened by dept q",
                               :sadf/contact "Fred Smith",
                               :sadf/contact-email "a@b.com"}}} (summarise-all memory-store)))
    (is (some #{{:sadf/name "test-model-2",
                 :sadf/uuid "124",
                 :sadf/summary "This is a test model with id 124",
                 :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                               :sadf/contact   "Not yet assigned",
                               :sadf/contact-email "z@b.com"}}} (summarise-all memory-store)))
    (is (= :no-result
          (update-summary memory-store "124" {:sadf/name "test-model-2",
                                              :sadf/uuid "124",
                                              :sadf/summary "This is a test model with id 124 And extras added",
                                              :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                                                            :sadf/contact "Not yet assigned",
                                                            :sadf/contact-email "z@b.com"}})))
    (is (some #{{:sadf/name "test-model-1",
                 :sadf/uuid "123",
                 :sadf/summary "This is a test model with id 123",
                 :sadf/detail {:sadf/ownership "Owened by dept q",
                               :sadf/contact "Fred Smith",
                               :sadf/contact-email "a@b.com"}}} (summarise-all memory-store)))
    (is (some #{{:sadf/name "test-model-2",
                 :sadf/uuid "124",
                 :sadf/summary "This is a test model with id 124 And extras added",
                 :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                               :sadf/contact   "Not yet assigned",
                               :sadf/contact-email "z@b.com"}}} (summarise-all memory-store)))

    (is (= true (delete-model memory-store "123")))
    (is (= '({:124 true}) (delete-all memory-store)))
    (is (= {:123 model-1} (update-model memory-store "123" model-1)))
    (is (= '({:123 true}) (delete-all memory-store)))
    (is (= '() (list-model-names memory-store))))

;Was a bug in delete all on the memory store with a changed model name
(deftest summaries
  (let [_ (delete-all memory-store)
        new1      (update-model memory-store "1234567" (assoc-in model-1 [:sadf/meta :sadf/name] "first-modelX"))
        id        (name (key (first new1)))
        summaries (summarise-all memory-store)]
    (is (= id  (:sadf/uuid (first summaries))))
    (is (= :no-result (update-summary memory-store id (:sadf/meta model-1))))
    (is (= (list {(keyword id) true}) (delete-all memory-store)))))


(deftest f-store
    (delete-all file-store)
    (is (= '() (list-model-names file-store)))
    (is (= :not-found (get-model file-store "123")))
    (is (= {":123" model-1} (update-model file-store "123" model-1)))
    (is (= "123" (get-in (get-model file-store "123") [:sadf/meta :sadf/uuid])))
    (is (= true (delete-model file-store "123")))
    (is (= '() (list-model-names file-store)))
    (is (= {":123" model-1} (update-model file-store "123" model-1)))
    (is (= {":124" model-2} (update-model file-store "124" model-2)))
    (is (some #{"123"} (list-model-names file-store)))
    (is (some #{"124"} (list-model-names file-store)))
    (is (some #{{:sadf/name "test-model-1",
                 :sadf/uuid "123",
                 :sadf/summary "This is a test model with id 123",
                 :sadf/detail {:sadf/ownership "Owened by dept q",
                               :sadf/contact "Fred Smith",
                               :sadf/contact-email "a@b.com"}}} (summarise-all file-store)))
    (is (some #{{:sadf/name "test-model-2",
                 :sadf/uuid "124",
                 :sadf/summary "This is a test model with id 124",
                 :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                               :sadf/contact   "Not yet assigned",
                               :sadf/contact-email "z@b.com"}}} (summarise-all file-store)))
    (is (= :no-result
          (update-summary file-store "124" {:sadf/name "test-model-2",
                                            :sadf/uuid "124",
                                            :sadf/summary "This is a test model with id 124 And extras added",
                                            :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                                                          :sadf/contact "Not yet assigned",
                                                          :sadf/contact-email "z@b.com"}})))
    (is (some #{{:sadf/name "test-model-1",
                 :sadf/uuid "123",
                 :sadf/summary "This is a test model with id 123",
                 :sadf/detail {:sadf/ownership "Owened by dept q",
                               :sadf/contact "Fred Smith",
                               :sadf/contact-email "a@b.com"}}} (summarise-all file-store)))
    (is (some #{{:sadf/name "test-model-2",
                 :sadf/uuid "124",
                 :sadf/summary "This is a test model with id 124 And extras added",
                 :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                               :sadf/contact   "Not yet assigned",
                               :sadf/contact-email "z@b.com"}}} (summarise-all file-store)))

    (is (= true (delete-model file-store "123")))
    (is (= '({:124 true}) (delete-all file-store)))
    (is (= {":123" model-1} (update-model file-store "123" model-1)))
    (is (= '({:123 true}) (delete-all file-store)))
    (is (= '() (list-model-names file-store))))


(deftest f-store-extra
  (let [_ (delete-all file-store)
        new1      (update-model file-store "123" (assoc-in model-1 [:sadf/meta :sadf/name] "first-modelX"))
        id        (-> (new1 ":123") :sadf/meta :sadf/uuid)
        summaries (summarise-all file-store)]
    (is (= id  (-> (first summaries) :sadf/uuid)))
    (is (= :no-result (update-summary file-store "456" (:sadf/meta model-1))))
    (is (= [{(keyword id) true}] (delete-all file-store)))))


;This test is here to track down a bug with the same operation sequence on the server side.
(deftest f-lists
  (delete-all file-store)
  (update-model file-store "123" model-1)
  (update-model file-store "124" model-2)
  (let [names       [(:sadf/name (first (summarise-all file-store))) (:sadf/name (second (summarise-all file-store)))]
        model-names (list-model-names file-store)
        deleted     (delete-all file-store)]
    (is (some #{"test-model-1"} names))
    (is (some #{"test-model-2"} names))
    (is (some #{"123"} model-names))
    (is (some #{"124"} model-names))
    (is (some #{{:123 true}} deleted))
    (is (some #{{:124 true}} deleted))))


(defn -main []
  (clojure.test/run-tests 'sa.sa-store.store-test 'sa.sa-store.couch-test))
