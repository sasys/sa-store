(ns sa.sa-store.couch-test
  "
    Test cases for the jvm based store operations
  "
  (:require [clojure.test                :refer [is deftest]]
            [sa.sa-store.store           :refer [summarise-all get-model list-model-names update-model delete-model update-summary delete-all]]
            [sa.sa-store.store-factory   :refer [get-store]]
            [sa.sa-store.couch-based     :refer [fix-context-types fix-context-type fix-flow-sources-and-sinks str->keyword fix-flow-source-sink]]))


(def couch-store (get-store :sa.sa-store.store-factory/couch-store))

(def model-1 {:sadf/meta  {:sadf/name "test-model-1"
                           :sadf/uuid "123"
                           :sadf/summary "This is a test model with id 123"
                           :sadf/detail {:sadf/ownership "Owened by dept q"
                                         :sadf/contact   "Fred Smith"
                                         :sadf/contact-email "a@b.com"}}
              :sadf/model {}
              :layout {(keyword "sadf" "1.2") [521 128],}})

(def model-2 {:sadf/meta {:sadf/name "test-model-2"
                          :sadf/uuid "124"
                          :sadf/summary "This is a test model with id 124"
                          :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note"
                                        :sadf/contact   "Not yet assigned"
                                        :sadf/contact-email "z@b.com"}}
              :sadf/model {}})

;This one has position information in it.
(def model-3 {:sadf/meta {:sadf/name "test-model-3"
                          :sadf/uuid "125"
                          :sadf/summary "This is a test model with id 125"
                          :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note"
                                        :sadf/contact   "Not yet assigned"
                                        :sadf/contact-email "z@b.com"}}
              :sadf/procs {}
              :sadf/layout {(keyword "sadf" "0") [450 450]}
              :sadf/flows  [{:sadf/id "2aa7be96-e790-4b95-8943-f5ca548c01b5",
                             :sadf/data [{:sadf/term {:label "pre-production-meta-data", :id "38dfa550-9be8-4a8e-8ee9-1a8b65e39380"}, :sadf/term-id "38dfa550-9be8-4a8e-8ee9-1a8b65e39380"}],
                             :sadf/source [{:sadf/posn [139 501], :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c", :sadf/proc-id :sadf/context}],
                             :sadf/sink [{:sadf/posn [319 455], :sadf/id "4604dfc7-e1f8-4799-88a3-b8023ef83258", :sadf/proc-id :sadf/context}],
                             :sadf/control-point [{:sadf/posn [279 518], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil} {:sadf/posn [279 518], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil}],
                             :sadf/description "Description of flow."}]
              :sadf/context-objects
                {:sadf/sources    [{:sadf/context-type :sadf/sources
                                    :sadf/id           "111"}]
                 :sadf/sinks      [{:sadf/context-type :sadf/sinks
                                    :sadf/id           "222"}]
                 :sadf/producers  [{:sadf/context-type :sadf/producers
                                    :sadf/id           "333"}]
                 :sadf/consumers  [{:sadf/context-type :sadf/consumers
                                    :sadf/id           "444"}]}})


;This one has flow source and sink as strings
(def model-4 {:sadf/meta {:sadf/name "test-model-3"}
                         :sadf/uuid "125"
                         :sadf/summary "This is a test model with id 125"
                         :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note"
                                       :sadf/contact   "Not yet assigned"
                                       :sadf/contact-email "z@b.com"}
              :sadf/procs {}
              :sadf/layout {(keyword "sadf" "0") [450 450]}
              :sadf/flows  [{:sadf/id "2aa7be96-e790-4b95-8943-f5ca548c01b5",
                             :sadf/data [{:sadf/term {:label "pre-production-meta-data", :id "38dfa550-9be8-4a8e-8ee9-1a8b65e39380"}, :sadf/term-id "38dfa550-9be8-4a8e-8ee9-1a8b65e39380"}],
                             :sadf/source [{:sadf/posn [139 501], :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c", :sadf/proc-id "sadf/context"}],
                             :sadf/sink [{:sadf/posn [319 455], :sadf/id "4604dfc7-e1f8-4799-88a3-b8023ef83258", :sadf/proc-id "sadf/context"}],
                             :sadf/control-point [{:sadf/posn [279 518], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil} {:sadf/posn [279 518], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil}],
                             :sadf/description "Description of flow."}]})

(def string-source-1  {:sadf/source [{:sadf/posn [139 501],
                                      :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c"
                                      :sadf/proc-id "sadf/context"}]})
(def keyword-source-1 {:sadf/source [{:sadf/posn [139 501],
                                      :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c"
                                      :sadf/proc-id :sadf/context}]})
(def nil-source-1     {:sadf/source [{:sadf/posn [139 501],
                                      :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c"
                                      :sadf/proc-id nil}]})

(def string-sink-1    {:sadf/sink [{:sadf/posn [139 501],
                                    :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c"
                                    :sadf/proc-id "sadf/1.1.2"}]})
(def keyword-sink-1   {:sadf/sink [{:sadf/posn [139 501],
                                    :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c"
                                    :sadf/proc-id (keyword "sadf" "1.1.2")}]})
(def nil-sink-1       {:sadf/sink [{:sadf/posn [139 501],
                                    :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c"
                                    :sadf/proc-id nil}]})

(def flow-1 {:sadf/source [{:sadf/posn [139 501],
                            :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c"
                            :sadf/proc-id "sadf/context"}]
             :sadf/sink   [{:sadf/posn [139 501],
                            :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c"
                            :sadf/proc-id "sadf/1.1.2"}]})



(deftest str->keyword-test
  (is (= (keyword "sadf" "context") (str->keyword "sadf/context")))
  (is (= (keyword "sadf" "1.0.1.1") (str->keyword "sadf/1.0.1.1")))
  (is (= :sadf/missing (str->keyword nil)))
  (is (= (keyword "/a") (str->keyword "/a")))
  (is (= :sadf/missing (str->keyword "a"))))


(deftest fix-flow-source-sink-test
  (is (= keyword-source-1 (fix-flow-source-sink string-source-1)))
  (is (= keyword-source-1 (fix-flow-source-sink keyword-source-1)))
  (is (= nil-source-1     (fix-flow-source-sink nil-source-1)))

  (is (= keyword-sink-1 (fix-flow-source-sink string-sink-1)))
  (is (= keyword-sink-1 (fix-flow-source-sink keyword-sink-1)))
  (is (= nil-sink-1     (fix-flow-source-sink nil-sink-1))))

(deftest fix-flow-sources-and-syncs-test
  (let [updated-model (fix-flow-sources-and-sinks model-3)
        updated-flow  (get-in updated-model [:sadf.flows 0])
        updated-source (get-in updated-flow [:sadf/source 0])
        updated-sink   (get-in updated-flow [:sadf/sink 0])]
    (is (= (:sadf/proc-id updated-source :sad/context)))
    (is (= (:sadf/proc-id updated-sink :sad/context)))))

(deftest fix-context-type-test
  (is (= {:sadf/context-type :sadf/consumers :sadf/name "fred"} (fix-context-type {:sadf/context-type "sadf/consumers"
                                                                                   :sadf/name         "fred"})))
  (is (= {:sadf/context-type :sadf/producers} (fix-context-type {:sadf/context-type "sadf/producers"})))
  (is (= {:sadf/context-type :sadf/sources}   (fix-context-type {:sadf/context-type "sadf/sources"})))
  (is (= {:sadf/context-type :sadf/sinks}     (fix-context-type {:sadf/context-type "sadf/sinks"}))))

(deftest fix-context-types-test
  (let [context-types {:sadf/context-objects {:sadf/sources   [{:sadf/context-type "sadf/sources"}]
                                              :sadf/sinks     [{:sadf/context-type "sadf/sinks"}]
                                              :sadf/producers [{:sadf/context-type "sadf/producers"}]
                                              :sadf/consumers [{:sadf/context-type "sadf/consumers"
                                                                :sadf/name         "fred"}]}}
        fixed-context-types {:sadf/context-objects {:sadf/sources   [{:sadf/context-type :sadf/sources}]
                                                    :sadf/sinks     [{:sadf/context-type :sadf/sinks}]
                                                    :sadf/producers [{:sadf/context-type :sadf/producers}]
                                                    :sadf/consumers [{:sadf/context-type :sadf/consumers
                                                                      :sadf/name         "fred"}]}}]
    (is (= fixed-context-types (fix-context-types context-types)))))


(deftest couch-store-test
  (delete-all couch-store)
  (is (= '() (list-model-names couch-store)))
  (update-model couch-store "123" model-1)
  (update-model couch-store "124" model-2)
  (let [names       [(:sadf/name (first (summarise-all couch-store))) (:sadf/name (second (summarise-all couch-store)))]
        model-names (list-model-names couch-store)
        deleted     (delete-all couch-store)]
    (is (some #{"test-model-1"} names))
    (is (some #{"test-model-2"} names))
    (is (some #{"123"} model-names))
    (is (some #{"124"} model-names))
    (is (some #{{:123 true}} deleted))
    (is (some #{{:124 true}} deleted))))


; (deftest couch-store-delete-then-summary-test
;   (delete-all couch-store)
;   (is (= '() (list-model-names couch-store)))
;   (update-model couch-store "123" model-1)
;   (doseq [id (range 10 100)]
;     (update-model couch-store (str id) model-2))
;   (let [deleted (delete-model couch-store "123")
;         summaries (summarise-all couch-store)]
;     (is (= 90 (count summaries)))
;     (print (last summaries))))


(deftest couch-store-summary-test
  (delete-all couch-store)
  (is (= '() (list-model-names couch-store)))
  (is (= {:123 model-1} (update-model couch-store "123" model-1)))
  (is (= {:124 model-2} (update-model couch-store "124" model-2)))
  (is (some #{{:sadf/name "test-model-1",
               :sadf/uuid "123",
               :sadf/summary "This is a test model with id 123",
               :sadf/detail {:sadf/ownership "Owened by dept q",
                             :sadf/contact "Fred Smith",
                             :sadf/contact-email "a@b.com"}}} (summarise-all couch-store)))

  (is (= "test-model-2-BBBB"
        (->
          (update-summary couch-store "124" {:sadf/name "test-model-2-BBBB",
                                             :sadf/uuid "124",
                                             :sadf/summary "This is a test model with id 124 And extras added",
                                             :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                                                           :sadf/contact "Not yet assigned",
                                                           :sadf/contact-email "z@b.com"}})
          :sadf/name)))

  (is (= "test-model-1-AAAA"
        (->
          (update-summary couch-store "123" {:sadf/name "test-model-1-AAAA",
                                             :sadf/uuid "123",
                                             :sadf/summary "This is a test model with id 123 And extras added",
                                             :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                                                           :sadf/contact "Not yet assigned",
                                                           :sadf/contact-email "z@b.com"}})
          :sadf/name)))

  (is (= :no-result
         (update-summary couch-store "999" {:sadf/name "test-model-1-AAAA",
                                            :sadf/uuid "123",
                                            :sadf/summary "This is a test model with id 123 And extras added",
                                            :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note",
                                                          :sadf/contact "Not yet assigned",
                                                          :sadf/contact-email "z@b.com"}}))))



(deftest couch-store-get-test
  (delete-all couch-store)
  (is (= '() (list-model-names couch-store)))
  (is (= {:125 model-3} (update-model couch-store "125" model-3)))
  (let [found     (get-model couch-store "125")
        flow      (first (:sadf/flows found))
        source    (get-in flow [:sadf/source 0])
        sink      (get-in flow [:sadf/sink 0])
        source-id (get-in source [:sadf/proc-id])
        sink-id   (get-in sink [:sadf/proc-id])
        contexts  (:sadf/context-objects found)
        sources   (:sadf/sources contexts)
        sinks     (:sadf/sinks contexts)
        producers (:sadf/producers contexts)
        consumers (:sadf/consumers contexts)]
    (is (= source-id :sadf/context))
    (is (= sink-id :sadf/context))
    (is (= :sadf/sources   (:sadf/context-type (first sources))))
    (is (= :sadf/sinks     (:sadf/context-type (first sinks))))
    (is (= :sadf/producers (:sadf/context-type (first producers))))
    (is (= :sadf/consumers (:sadf/context-type (first consumers))))))


(deftest couch-store-delete-then-get-test
  (delete-all couch-store)
  (is (= {:125 model-3} (update-model couch-store "125" model-3)))
  (let [found (get-model couch-store "125")]
    (is found)
    (let [deleted (delete-model couch-store "125")]
      (is (= (:ok deleted) true))
      (is (nil? (get-model couch-store "not-me"))))))
