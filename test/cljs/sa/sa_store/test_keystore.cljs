(ns sa.sa-store.test-keystore
 
    "
     Contains cljs-test-display based  tests against browser keystore.

     The async test code is found on stackoverflow here :
     https://stackoverflow.com/questions/30766215/how-do-i-unit-test-clojure-core-async-go-macros
    "

    (:require [cljs.test :as t :refer [is testing async report deftest  run-tests use-fixtures]]
              [cljs-test-display.core]
              [sa.sa-store.test-git-url]
              [sa.sa-store.store-factory    :as sf]
              [sa.sa-store.store            :as st]
              [sa.sa-store.browser-keystore :as bks]
              [cljs.core.async           :refer [go-loop alts! take! put! chan <! >! timeout close! ] :refer-macros [go] :as async]))

(enable-console-print!)

(def TEST-TIMEOUT 2000) ;ms Test should take no linger than this

(def model-1  {:sadf/meta  {:name "test-model-1"
                            :uuid "123"
                            :summary "This is a test model with id 123"
                            :detail {:ownership "Owened by dept q"
                                     :contact   "Fred Smith"
                                     :contact-email "a@b.com"}}
               :model {}})

(def model-2  {:sadf/meta  {:name "test-model-2"
                            :uuid "124"
                            :summary "This is a test model with id 124"
                            :detail {:ownership "Owened by dept z bur soon to change long note"
                                     :contact   "Not yet assigned"
                                     :contact-email "z@b.com"}}
               :model {}
               :layout {(keyword "sadf" "1.2.3") [22,22],
                        (keyword "sadf" "a2222-33-b44") [99,99]}})



(def model-3  {:sadf/meta  {:name "test-model-2"
                            :uuid "125"
                            :summary "This is a test model with id 124"
                            :detail {:ownership "Owened by dept z bur soon to change long note"
                                     :contact   "Not yet assigned"
                                     :contact-email "z@b.com"}}
               :sadf/procs {}
               :sadf/dictionary {}
               :sadf/flows []
               :sadf/context-objects {}
               :layout {(keyword "sadf" "11122") [521 128],
                        (keyword "sadf" "1.2") [522,522]}})

;:layout {(keyword "sadf" "1.2.3") [22,22], (keyword "sadf" "a2222-33-b44")}


(def model-1-id "123")
(def model-2-id "124")
(def model-3-id "125")

(def summary-1
      {:name "test-model-1",
       :uuid "123",
       :summary "This is a test model with id 123",
       :detail {:ownership "Owened by dept q"
                :contact   "Fred Smith"
                :contact-email "a@b.com"},
       :uri "sbnl.sa.123"})

(def summary-2
      {:name "test-model-2",
       :uuid "124",
       :summary "This is a test model with id 124",
       :detail {:ownership "Owened by dept z bur soon to change long note"
                :contact   "Not yet assigned"
                :contact-email "z@b.com"},
       :uri "sbnl.sa.124"})

(def updated-summary-2
     {:name "A New Model",
      :uuid "124",
      :summary "This is a test model with id 124",
      :detail {:ownership "Owened by dept z bur soon to change long note"
               :contact   "Not yet assigned"
               :contact-email "z@b.com"},
      :uri "sbnl.sa.124"})

(def log (.-log js/console))


(defn test-async
  "Asynchronous test awaiting ch to produce a value or close."
  [ch]
  (async done
   (take! ch (fn [_] (done)))))

(defn test-within
  "Asserts that ch does not close or produce a value within ms. Returns a
  channel from which the value can be taken."
  [ms ch]
  (go (let [t (timeout ms)
            [v ch] (alts! [ch t])]
        (is (not= ch t)
            (str "Test should have finished within " ms "ms."))
        v)))


;SEE https://stackoverflow.com/questions/34560423/how-to-exhaust-a-channels-values-and-then-return-the-result-clojurescript
(defn read-all [from-chan]
  (go-loop [res []]
    (let [[v _] (alts! [from-chan] :default :complete)]
      (if (= v :complete)
        res
        (recur (conj res v))))))


(defn delete-models
  ([]
   (log "deleteing all models")
   (let [ks   (sf/get-store ::sf/key-store)
         list-chan    (chan)
         del-chan (chan 100)]
     (st/list-model-names ks list-chan)
     (go-loop [name (<! list-chan)]
        (if bks/is-sa-model? name
          (st/delete-model ks del-chan name)))
     del-chan)))


(defn before-tests []
  (delete-models))


(defn after-tests []
  (delete-models))


(use-fixtures :each
  {:before before-tests
   :after after-tests})


(def test-keys {(keyword "sadf" "2.1") [1,1]
                (keyword "sadf" "1222-af-22") [2,2]
                (keyword "sadf" "2.2") (keyword "sadf" "333-aa-df")})


(deftest model-filters
  (testing "We can filter single models from non-models in the store"
    (is (bks/is-sa-model? "sbnl.sa.my-model"))
    (is (not (bks/is-sa-model? "no-not-me-sbnl.sa.my-model"))))
  (testing "We can filter models from non-models in the store"
    (is (= '("sbnl.sa.my-model") (bks/are-models? ["sbnl.sa.my-model" "no-not-me-sbnl.sa.my-model"])))))





(deftest layout-keys-test
  (testing "Convert layout keys to / from strings on the browser keystore directly"
    (let [s (bks/layout-keys-to-string test-keys)
          k (bks/layout-keys-from-string s)]
      (is (= k test-keys))))
  (testing "Convert layout section of model"
    (let [s (bks/model-layout-keys-to-str model-3)
          k (bks/model-layout-keys-from-str s)]
      (is (= k model-3))))
  (testing "Convert empty layout section of model"
    (let [rm (assoc model-3 :layout {})
          s (bks/model-layout-keys-to-str rm)
          k (bks/model-layout-keys-from-str s)]
      (is (= k rm))))
  (testing "Convert odd layout section of model"
    (let [rm (assoc model-3 :layout {(keyword "sadf" "1.2.3") [22,22], (keyword "sadf" "a2222-33-b44") [99,99]})
          s (bks/model-layout-keys-to-str rm)
          k (bks/model-layout-keys-from-str s)]
      (is (= k rm)))))




(deftest delete
  (testing "Testing delete all models"
    (let [ks   (sf/get-store ::sf/key-store)
          list-chan    (chan)
          del-chan    (chan)
          update-chan (chan)]
      (st/update-model ks update-chan model-1-id model-1)
      (test-async
        (go (is (= {:123 model-1} (<! update-chan)) "The model was stored and returned")))
      (st/list-model-names ks list-chan)
      (test-async
        (go (let [names (<! list-chan)]
              (for [name names]
                (st/delete-model ks del-chan name))
              (is (= (<! (read-all del-chan)) []))))))))

(deftest test-model-not-found
  "
    Test we get the expected behaviour if the model we try and find does not exist.
  "
  (testing "no such model"
    (let [key-store   (sf/get-store ::sf/key-store)
          get-chan (chan)]
      (st/get-model key-store get-chan "no-such-model")
      (test-async
        (test-within TEST-TIMEOUT
          (go (is (= :sa.sa-store.browser-keystore/not-found (<! get-chan)) "Model not found")))))))


(deftest keystore-test-1
 "
  ## Testing Create Get Delete Get.
  Test we can create a new model, get the model, delete the model
  and verify it has been removed.
 "
  (testing "Testing create get delete get"
    (let [key-store   (sf/get-store ::sf/key-store)
          update-chan (chan)
          get-chan    (chan)
          del-chan    (chan)]

      (st/update-model key-store update-chan model-1-id model-1)
      (test-async
        (go (is (= {:123 model-1} (<! update-chan)) "The model was stored and returned")))
      (st/get-model key-store  get-chan model-1-id)
      (test-async
        (go (is (= model-1 (<! get-chan)) "The expected model is present")))
      (st/delete-model key-store del-chan model-1-id)
      (test-async
        (go (is (= true (<! del-chan)) "Deleted ok")))
      (st/get-model key-store get-chan model-1-id)
      (test-async
        (test-within TEST-TIMEOUT
          (go (is (= :sa.sa-store.browser-keystore/not-found (<! get-chan)) "Model is no longer present"))))
      (st/update-model key-store update-chan model-3-id model-3)
      (test-async
        (go (is (= {:125 model-3} (<! update-chan)) "The model was stored and returned")))
      (st/get-model key-store  get-chan model-3-id)
      (test-async
        (go (is (= model-3 (<! get-chan)) "The expected model is present")))
      (st/delete-model key-store del-chan model-3-id)
      (test-async
        (go (is (= true (<! del-chan)) "Deleted ok"))))))

(deftest keystore-test-2
 "
  ## Testing Create Get Update Get
  Test we can create a new model, get the model, update the model and get
  the updated mode.
 "
  (testing "Testing create get update get"
    (let [key-store       (sf/get-store ::sf/key-store)
          updated-model-2 (assoc-in model-2 [:meta :summary] "Nope")
          get-chan        (chan)
          update-chan     (chan)]

      (st/update-model key-store update-chan model-2-id model-2)
      (test-async
        (go (is (= {:124 model-2} (<! update-chan)) "The model was stored and returned")))
      (st/get-model key-store get-chan model-2-id)
      (test-async
        (go (is (= model-2 (<! get-chan)) "The expected model was present")))
      (st/update-model key-store update-chan  model-2-id updated-model-2)
      (test-async
        (go (is (= {:124 updated-model-2} (<! update-chan)) "The model was updated")))
      (st/get-model key-store get-chan model-2-id)
      (test-async
        (go (is (= updated-model-2 (<! get-chan)) "The updated model was present"))))))



(deftest keystore-test-3
  "
   ## Testing List Model Names
   Test we can list the model names.
  "
   (testing "Testing list model names"
    (let [key-store   (sf/get-store ::sf/key-store)
          update-chan (chan 5)
          list-chan   (chan)]

      (st/update-model key-store update-chan model-1-id model-1)
      (test-async
        (go (is (= {:123 model-1} (<! update-chan)) "The model 1 was stored and returned")))
      (st/update-model key-store update-chan model-2-id model-2)
      (test-async
        (go (is (= {:124 model-2} (<! update-chan)) "The model 2 was stored and returned")))
      (st/list-model-names key-store list-chan)
      (test-async
        (go
          (let [res (<! list-chan)]
        ;(go (is (= '("123" "124") (<! list-chan)) "The model list was correct"))))))
            (is (and (some #{"123"} res) (some #{"124"} res)))))))))


(deftest keystore-test-4
  "
   ## Testing Model Summaries
   Test we can get the model summaries.
  "
   (testing "Testing model summaries"
    (delete-models)
    (let [key-store   (sf/get-store ::sf/key-store)
          update-chan (chan 5)
          sum-chan    (chan)]

      (st/update-model key-store update-chan model-1-id model-1)
      (test-async
        (go (is (= {:123 model-1} (<! update-chan)) "The model 1 was stored and returned")))
      (st/update-model key-store update-chan model-2-id model-2)
      (test-async
        (go (is (= {:124 model-2} (<! update-chan)) "The model 2 was stored and returned")))
      (st/summarise-all  key-store sum-chan)
      (test-async
        (go
          (let [summaries (<! sum-chan)]
            (is (and (some  #{{:name "test-model-1",
                               :uuid "123",
                               :summary "This is a test model with id 123",
                               :detail {:ownership "Owened by dept q",
                                        :contact "Fred Smith",
                                        :contact-email "a@b.com",}
                               :sadf/uri "sbnl.sa.123"}} summaries)
                     (some #{ {:name "test-model-2",
                               :uuid "124",
                               :summary "This is a test model with id 124",
                               :detail {:ownership "Owened by dept z bur soon to change long note",
                                        :contact "Not yet assigned",
                                        :contact-email "z@b.com",}
                               :sadf/uri "sbnl.sa.124"}} summaries)))))))))


(deftest keystore-test-5
 "
  ## Testing Update Model Summaries
  Test we can update model summaries.
 "
 (testing "Testing update  model summaries"
   (let [key-store       (sf/get-store ::sf/key-store)
         update-chan     (chan)
         update-sum-chan (chan)]

      (st/update-model key-store update-chan model-1-id model-1)
      (test-async
        (go (is (= {:123 model-1} (<! update-chan)) "The model 1 was stored and returned")))
      (st/update-model key-store update-chan model-2-id model-2)
      (test-async
        (go (is (= {:124 model-2} (<! update-chan)) "The model 2 was stored and returned")))
      ;THE UPDATE HERE FAILS, FIX THIS
      (st/update-summary key-store update-sum-chan model-2-id updated-summary-2)
      (test-async
        (go (is (= (<! update-sum-chan) {:124 {:sadf/meta
                                                 {:name "A New Model",
                                                  :uuid "124",
                                                  :summary "This is a test model with id 124",
                                                  :detail {:ownership "Owened by dept z bur soon to change long note",
                                                           :contact "Not yet assigned",
                                                           :contact-email "z@b.com",}
                                                  :uri "sbnl.sa.124",}
                                               :model {}
                                               :layout {(keyword "sadf" "1.2.3") [22,22],
                                                        (keyword "sadf" "a2222-33-b44") [99,99]}}}) "The summary has been updated")))
      (st/update-summary key-store update-sum-chan "no-id" updated-summary-2)
      (test-async
          (go (is (= :sa.sa-store.browser-keystore/not-found (<! update-sum-chan)) "The model was not found"))))))


(deftest keystore-test-6
 "
  ## Test Get Summaries In Polluted Keystore
 "
 (testing "Testing update  model summaries"
     (delete-models)
     (.setItem (.-localStorage js/window) "Some-Name.not-mine" "#{[\"app-db-path\" nil 0 0 0] [\"app-db-path\" nil 0 0 0 :body 0 6 1 3]}")

     (let [key-store       (sf/get-store ::sf/key-store)
           update-chan     (chan)
           update-sum-chan (chan)
           sum-chan    (chan)]

        (st/update-model key-store update-chan model-1-id model-1)
        (test-async
          (go (is (= {:123 model-1} (<! update-chan)) "The model 1 was stored and returned")))
        (st/update-model key-store update-chan model-2-id model-2)
        (test-async
          (go (is (= {:124 model-2} (<! update-chan)) "The model 2 was stored and returned")))
        (st/summarise-all  key-store sum-chan)
        (test-async
          (go
            (let [summaries (<! sum-chan)]
              (is (and (some  #{{:name "test-model-1",
                                 :uuid "123",
                                 :summary "This is a test model with id 123",
                                 :detail {:ownership "Owened by dept q",
                                          :contact "Fred Smith",
                                          :contact-email "a@b.com",}
                                 :sadf/uri "sbnl.sa.123"}} summaries)
                       (some #{ {:name "test-model-2",
                                 :uuid "124",
                                 :summary "This is a test model with id 124",
                                 :detail {:ownership "Owened by dept z bur soon to change long note",
                                          :contact "Not yet assigned",
                                          :contact-email "z@b.com",}
                                 :sadf/uri "sbnl.sa.124"}} summaries)) "")
              (is (= 2 (count summaries)))))))))





;Figwheel support
(defn test-run []
  (cljs.test/run-tests
    (cljs-test-display.core/init! "app")
    'sa.sa-store.test-keystore))
    ;'sa.sa-store.test-git-url))


(defn ^:export init
  ([]
   (test-run)))
