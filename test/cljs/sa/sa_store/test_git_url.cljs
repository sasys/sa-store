(ns sa.sa-store.test-git-url

    "
     Contains cljs-test-display based  tests against git repo store.

     The async test code is found on stackoverflow here :
     https://stackoverflow.com/questions/30766215/how-do-i-unit-test-clojure-core-async-go-macros
    "

    (:require [cljs.test :as t :refer [is testing async report deftest  run-tests use-fixtures]]
              [sa.sa-store.store-factory :as sf]
              [sa.sa-store.store         :as st]
              [cljs.core.async           :refer [go-loop alts! take! put! chan <! >! timeout close!] :refer-macros [go]]))

(def TEST-TIMEOUT 2000)

(enable-console-print!)


(defn read-all [from-chan]
        (loop [res []]
            (let [[v _] (alts! [from-chan] :default :complete)]
              (if (or (= v :default) (= v :complete))
                res
                (recur (conj res v))))))


(defn test-async
  "Asynchronous test awaiting ch to produce a value or close."
  [ch]
  (async done
   (take! ch (fn [_] (done)))))

(defn test-within
  "Asserts that ch does not close or produce a value within ms. Returns a
  channel from which the value can be taken."
  [ms ch]
  (go (let [t (timeout ms)
            [v ch] (alts! [ch t])]
        (is (not= ch t)
            (str "Test should have finished within " ms "ms."))
        v)))

(deftest keystore-test-1
 "
  ## Testing  get by url
  Test we can get the test transit model from the test bitbucket location.
 "
  (testing "Testing get"
    (let [git-url-store   (sf/get-store ::sf/git-url)
          get-chan    (chan)
          u "https://bitbucket.org/sasys/sa-test-data/src/master/resources/sbnl.sa.1177bd50-7059-4033-996b-cc309bc24838.transit"]
      (st/get-model-by-uri git-url-store get-chan u)
      ;(test-async
        ;(go (print (read-all get-chan)))))))
      (test-async (test-within TEST-TIMEOUT (go (is (not (nil? (<! get-chan))) "The expected model is present")))))))


(deftest keystore-test-2
 "
  ## Testing  Get url that the model is normal
  Test that the model is normal
 "
  (testing "Testing get"
    (let [git-url-store   (sf/get-store ::sf/git-url)
          get-chan    (chan)
          u "https://bitbucket.org/sasys/sa-test-data/src/master/resources/sbnl.sa.1177bd50-7059-4033-996b-cc309bc24838.transit"]
      (st/get-model-by-uri git-url-store get-chan u)
      ;(test-async
        ;(go (print (read-all get-chan)))))))
      (test-async (test-within TEST-TIMEOUT (go
                                              (let [model  (<! get-chan)]
                                                (is (not (nil? (:sadf/procs model)))))))))))
