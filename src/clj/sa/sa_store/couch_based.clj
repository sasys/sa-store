(ns sa.sa-store.couch-based
  "
    Server side model storage in Couch.

    Requires env vars for:
    DB_HOST (eg \"razor\")
    DB_PORT (eg 5984)
    DB_NAME (eg \"sa\")

  "
  (:require [com.ashafa.clutch       :as cl]
            [com.ashafa.clutch.utils :as clu]
            [clojure.string          :as str]
            [environ.core            :refer [env]]
            [sa.sa-store.store       :refer [SaStore delete-model get-model update-model]]
            [taoensso.timbre         :refer [info  warn  error  fatal]]))

;TODO REMOVE THIS!!
(def DEFAULT_ADMIN "admin")
(def DEFAULT_PASSWORD "password")

(def DEFAULT_DB_PORT "5984")
(def DEFAULT_DB_HOST "razor")
(def DEFAULT_DB_NAME "sa")

(def DB_PORT (Integer/parseInt (or (env :db-port)
                                   DEFAULT_DB_PORT)))

(def DB_USER (or (env :db-user)
                 DEFAULT_ADMIN))

(def DB_PASSWORD (or (env :db-password)
                     DEFAULT_PASSWORD))

(def DB_HOST (or (env :db-host)
                 DEFAULT_DB_HOST))

(def DB_NAME (or (env :db-name)
                 DEFAULT_DB_NAME))

(def db (assoc (clu/url (str "http://" DB_HOST)) :port DB_PORT :path (str "/" DB_NAME) :username DB_USER, :password DB_PASSWORD))

;Here we just want to make sure the db is created.
(try
  (cl/create-database db)
  (catch Exception ex (str "caught exception " ex)))


(defn make-db []
  (try
    (cl/create-database db)
    true
    (catch Exception ex (str "caught exception " ex)
      false)))


(defn str->keyword
  "
  Given a string version of keyword, safley make a keyword from it.
  If there us no keyword to be made, return :sadf/missing
  "
  [name]
  (if name
    (let [[k v] (str/split name #"\/" 2)]
      (cond
        (true? (and (some? k) (some? v))) (keyword k v)
        (true? (and (nil? k) (some? v)))  (keyword v)
        :else                             (keyword :sadf/missing)))
    (keyword :sadf/missing)))


(defn fix-context-type
  "Function alters any string version of a context type to a keyword.
   This seems to happen with couch."
  [context-object]
  (assoc-in context-object [:sadf/context-type] (str->keyword (:sadf/context-type context-object))))


(defn fix-context-types
  "
    Given a  model obtained from couchdb we check the context type of each context object
    to make sure it is a keyword and convert it to a key word if required,
    couch returns them as strings.
  "
  [model]
  (assoc-in model [:sadf/context-objects]
    (into {}
      (for [key (keys (:sadf/context-objects model))]
        {key
          (map fix-context-type (get-in model [:sadf/context-objects key]))}))))


(defn fix-flow-source-sink
  "Function alters any string version of a flow source or sink to a keyword.
   This seems to happen with couch."
  [flow]
  (let [source (get-in flow [:sadf/source 0 :sadf/proc-id])
        sink   (get-in flow [:sadf/sink   0 :sadf/proc-id])
        fixed-flow  (cond
                      (nil?     source) flow
                      (keyword? source) flow
                      :else (assoc-in flow [:sadf/source 0 :sadf/proc-id] (str->keyword source)))]
    (cond
      (nil?     sink) fixed-flow
      (keyword? sink) fixed-flow
      :else (assoc-in fixed-flow [:sadf/sink 0 :sadf/proc-id] (str->keyword sink)))))


(defn fix-flow-sources-and-sinks
  "
    Given a  model obtained from couchdb we check the source and sink of each flow
    to make sure it is a keyword, couch returns them as strings.
  "
  [model]
  (assoc model :sadf/flows (map fix-flow-source-sink (:sadf/flows model))))


(defn get-model-filter
  " Allows us to fix any issues in the model after reading."
  [name f]
  (if f
    (f (:data (cl/with-db db (cl/get-document name))))
    (:data (cl/with-db db (cl/get-document name)))))

(def couch-store
  (reify SaStore

    (list-model-names [this]
      (info "List model names")
      (map :key (cl/all-documents db)))

    (update-model [this name model]
        (info (str "Update document : " name))
        (if (cl/document-exists? db name)
          (let [{:keys [data _id _rev]} (-> (cl/get-document db name))]
            {(keyword name) (:data (cl/put-document db {:_id name :_rev _rev :data model}))})
          {(keyword name) (:data (cl/put-document db {:_id name :data model}))}))

    (get-model [this name]
      (if (cl/with-db db (cl/document-exists? name))
        (-> (:data (cl/with-db db (cl/get-document name)))
          fix-flow-sources-and-sinks
          fix-context-types)
        nil))

    (delete-model [this name]
      (info (str "Delete document : " name))
      (->> name
        (cl/get-document db)
        (cl/delete-document db)))

    (delete-all [this]
        (doall (for [key (map :key (cl/all-documents db))]
                (let [r (delete-model this key)]
                  {(keyword (:id r)) (:ok r)}))))

    (summarise-all [this]
      (remove nil?
        (for [key (map :key (cl/all-documents db))]
          (-> (cl/get-document db key)
              :data
              :sadf/meta))))

    (update-summary [this uuid summary]
      (info (str "update-summary -> Updating summary for uuid : " uuid " and summary update " summary))
      (if (cl/document-exists? db uuid)
        (if-let [{:keys [data _id _rev]} (-> (cl/get-document db uuid))]
          (let [model  (assoc data :sadf/meta summary)]
            (cl/put-document db {:_id uuid :_rev _rev :data model})
            (:sadf/meta  (-> (cl/get-document db uuid)
                             :data)))
          :no-result)
        :no-result))))
