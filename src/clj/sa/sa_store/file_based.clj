(ns sa.sa-store.file-based
  "
   An sa store implementation based on the file system.
   Uses a hidden directory in the users home location
   with a file structure that uses the project name
   to name the directory.  The file itself is called
   name.sa

   Implementation uses core async to serialise changes to the containing directory.

   Each operation is bundled up with its parameters into a vector and passed
   into a channel where a single thread removes these bundles one at a time
   and executes them.  Responses are placed on the provided return channel.

   Return valuse are not consistent with other implementations - needs some work!!

  "
  (:require [sa.sa-store.store  :refer [SaStore]]
            [os.details         :as os]
            [clojure.edn        :as edn]
            [clojure.string     :refer [ends-with? starts-with?]]
            [clojure.core.async :as a
                                :refer [>! <! >!! <!! go chan buffer close! thread
                                        alts! alts!! timeout go-loop]]
            [clojure.java.io    :as io]
            [taoensso.timbre    :refer [info  warn  error  fatal]])
  (:import  java.io.File))

(def model-1 {:sadf/meta  {:sadf/name "Test Model 1"
                           :sadf/uuid "123"
                           :sadf/summary "This is a test model with id 123"
                           :sadf/detail  {:sadf/ownership "Owened by dept q"
                                          :sadf/contact   "Fred Smith"
                                          :sadf/contact-email "a@b.com"}}
              :sadf/model {}})

(def model-2 {:sadf/meta  {:sadf/name "Test Model 2"
                           :sadf/uuid "124"
                           :sadf/summary "This is a test model with id 124"
                           :sadf/detail {:sadf/ownership "Owened by dept z bur soon to change long note"
                                         :sadf/contact   "Not yet assigned"
                                         :sadf/contact-email "z@b.com"}}
              :sadf/model {}})



;The hidden directory to store the sa file system in
(def store-base ".sa")

;The suffix of the model file
(def model-suffix "sa")

(def dir-prefix "model-")

(defn make-dir-name
  ([name]
   "Makes the directory name to store a specific model."
   (str (os/user-dir) "/" store-base "/" dir-prefix name))
  ([name suffix]
   "Makes the directory name to store a specific model."
   (str (os/user-dir) "/" store-base "/" dir-prefix name "." suffix))
  ([]
   "Makes the base directory name to store models."
   (str (os/user-dir) "/" store-base)))


(defn make-file-name
   "Makes the file name for the model name."
  [name]
  (str name "." model-suffix))


(defn remove-suffix
  [name]
  (apply str
    (if (ends-with? name model-suffix)
      (drop-last 3 name)
      name)))

(defn remove-prefix
  [name]
  (apply str
    (if (starts-with? name dir-prefix)
      (drop (count dir-prefix) name)
      name)))


(defn list-model-names
  "List all available model names."
  []
  (let [files (filter #(ends-with? (.getName %) model-suffix) (-> (make-dir-name)
                                                                  File.
                                                                  .listFiles))]
    (doall (map #(remove-prefix (remove-suffix (.getName %))) files))))


(def op-chan (chan 10))

(defn file-ops
 "Serialises operations on the file system"
 ([op-chan]
  (go-loop []
       (let [packet (<! op-chan)
             f      (first packet)
             resp   (second packet)]
           (info "file-ops -> packet : " packet)
           (>! resp
              (if-let [result
                       (if-let [args   (seq (drop 2 packet))]
                          (apply f args)
                          (f))]
                result
                :no-result)))
     (recur)))
 ([]
  (file-ops op-chan)))


(file-ops)


(defn get-model
  "Get the model if it exists else return :not-found"
  [name]
  (if (.exists (io/as-file (make-dir-name name model-suffix)))
    (edn/read-string (slurp (make-dir-name name model-suffix)))
    :not-found))

(defn update-model
  "If the model doesnt exist, create it else update it.
   then read it back and return it."
  [name model]
  (info (str "update-model -> Params :name  " name " model : " model))
  (let [dir-name (make-dir-name name model-suffix)]
     (clojure.java.io/make-parents dir-name)
     (spit dir-name model))
  (let [updated-model (get-model name)]
    (if (= :not-found updated-model)
        (error (str "Did not find updated model with id : " name))
        (info (str "Found updated model with id : " name)))
    {(str (keyword name)) updated-model})) ;we need to string the keyword,
                                           ;the edn reader doesnt like the leading key}))



(defn delete-model
  [name]
  (let [model-name (make-dir-name name model-suffix)]
      (try
        (info "Attempting to delete model : " model-name)
        (clojure.java.io/delete-file model-name)
        (catch java.io.IOException ioex
          (do (error "Error trying to delete model : " name " was " (.getMessage ioex)))
          :error)
        (finally (info "Deleted model : " name)))))




(defn delete-all
  []
  (doall
    (for [name (list-model-names)]
     (do
      (info "About to delete id : " name)
      {(keyword name) (delete-model name)}))))


(defn summarise-all
  "Returns a list of the model summaries."
  []
  (if-let [models (for [name (list-model-names)]
                    (get-model name))]
    (do
      (info "Found models : " models)
      (map (fn [amodel] (amodel :sadf/meta)) models))
    (do
      (info "No models found")
      [])))

(defn update-summary
  [uuid summary]
  (info (str "update-summary -> Updating summary for uuid : " uuid " and summary update " summary))
  (let [model (get-model uuid)]
    (info (str "update-summary ->  Found model : " model))
    (if-not (= :not-found model)
      (update-model uuid (assoc model :sadf/meta summary)))
    nil))




(defmacro serialise-access
  "
   Use this macro to serialise access to the file based operations.
   f  function to invoke
   o  output channel to put the result on
   ps optional parameters to the function
  "
  [f o & ps]
  `(let [r# (chan)]
    (go (>! ~o [~f r# ~@ps]))
    (<!! r#)))



(def file-store
  (reify SaStore
    (list-model-names [this]
        (serialise-access list-model-names op-chan))

    (update-model [this name model]
        (serialise-access update-model op-chan name model))

    (get-model [this name]
          (serialise-access get-model op-chan name))

    (delete-model [this name]
        (serialise-access delete-model op-chan name))

    (delete-all [this]
        (serialise-access delete-all op-chan))

    (summarise-all [this]
      (let [ret-chan (chan)]
        (go (>! op-chan [summarise-all ret-chan]))
        (<!! ret-chan)))
      ;(serialise-access summarise-all op-chan))

    (update-summary [this uuid summary]
      (serialise-access update-summary op-chan uuid summary))))
