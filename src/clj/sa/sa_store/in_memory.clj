(ns sa.sa-store.in-memory
  "An sa store implementation based in memory.   "
  (:require [sa.sa-store.store :refer [SaStore]]))

(def memory (ref {}))

(def memory-store
  (reify SaStore
    (list-model-names [this] (map name (keys @memory)))
    (update-model [this name model]
                 (let [id     name
                       updated-model (assoc-in model [:sadf/meta :sadf/uuid] id)]
                   (dosync (alter memory assoc (keyword id) updated-model) {(keyword name) updated-model})))
    (get-model [this name] (if-let [res (get-in @memory [(keyword name)])] res :not-found))
    (delete-model [this name] (dosync (alter memory dissoc   (keyword name)) true))
    (delete-all [this]
      (let [names (keys @memory)]
        (dosync (alter memory empty))
        (map #(hash-map % true) names)))

    (summarise-all
     [this]
     (for [name (keys @memory)]
       (get-in @memory [name :sadf/meta])))

    (update-summary
     [this uuid summary]
     (if-let [_    (-> (filter #(= uuid (get-in % [:sadf/meta :sadf/uuid])) (vals @memory))
                       (first)
                       (:sadf/meta)
                       (:sadf/name)
                       (keyword))]
       (do (dosync (alter memory assoc-in [(keyword uuid)  :sadf/meta] summary)) :no-result)
       :not-found))))
