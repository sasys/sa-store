(ns sa.loader
  "
   Just so we can force cojure to download its deps
   clj -m sa.loader :demo-build :test or :prod-build for prod build

   to run the tests you need karma ie npm install karma --save-dev

   This is based off and heavliy copied from clj-chrome-devtools
   See https://github.com/tatut/clj-chrome-devtools
   And specifically https://github.com/tatut/clj-chrome-devtools/blob/master/src/clj_chrome_devtools/cljs/test.clj

  "
  (:require [clojure.tools.deps.alpha               :as deps]
            [cljs.build.api                         :as cljs]
            [clojure.string                         :as str]
            [clojure.java.io                        :as io]
            [clj-chrome-devtools.cljs.test          :refer [build-and-test run-tests build]]
            [clj-chrome-devtools.automation.fixture :refer [create-chrome-fixture]]
            [clj-chrome-devtools.impl.util          :refer [random-free-port]]
            [clj-chrome-devtools.automation         :as automation]
            [org.httpkit.server                     :as http-server])
  (:import (java.io File))

  (:gen-class))

(def common-deps {:mvn/repos {"central" {:url "https://repo1.maven.org/maven2/"}
                              "clojars" {:url "https://repo.clojars.org/"}}})



(def req-deps  {:deps {'com.sbnl.sa.sa-store/sa-store  {:git/url "https://sbnlocean@bitbucket.org/sasys/sa-store.git"
                                                        :sha "c68676fe97104b083ebdf7c43f2fa75ba667ec38"
                                                        :deps/manifest :deps}
                       'org.clojure/clojure             {:mvn/version "1.10.0"}
                       'org.clojure/clojurescript       {:mvn/version "1.10.439"}
                       'clj-chrome-devtools             {:mvn/version "20190329"}
                       'javax.xml.bind/jaxb-api         {:mvn/version "2.4.0-b180830.0359"}}
                :paths ["src/cljs" "test/cljs"]})


(defn prod-build []
   (cljs/build ["src/cljs"]
               {:output-dir           "tmp" ;"resources/public/js"
                :output-to            "resources/public/js/explorer.js"
                :main                 "sa.explorer.core"
                :asset-path           "js"
                :pretty-print false
                :optimizations :advanced}))

(defn demo-build []
     (cljs/build "src/cljs"
                 {:output-dir           "resources/public/js"
                  :asset-path           "resources/public/js"
                  :optimizations :none
                  :source-map true
                  :source-map-timestamp true
                  :cache-analysis true})
     (cljs/build "test/cljs"
                 {:output-dir           "resources/public/js"
                  :output-to            "resources/public/js/test_keystore.js"
                  :main                 "sa.explorer.test-runner"
                  :asset-path           "resources/public/js"
                  :optimizations :none
                  :source-map true
                  :source-map-timestamp true
                  :cache-analysis true}))


(defn- test-runner-forms
  "ClojureScript forms for test runner"
  [namespaces]
  (str/join
   ["(ns clj-chrome-devtools-runner \n"
    "  (:require [cljs.test :refer [run-tests]]\n"
    (str/join (map #(str "            [" % "]\n") namespaces))
    "))\n"
    "(def PRINTED (atom []))\n"
    "(defn get-printed [] "
    "  (let [v @PRINTED] "
    "    (reset! PRINTED []) "
    "    (clj->js v)))\n"
    "(defn run-chrome-tests []"
    " (set! *print-fn* (fn [& msg] (swap! PRINTED conj (apply str msg))))\n"
    "(run-tests " (str/join " "
                              (map #(str "'" %) namespaces)) "))"]))


(defn- with-test-runner-source [namespaces source-path fun]
  ;; Create a test runner source file in the given source path
  ;; We have to put this in an existing source path as
  ;; we can't add a new source path dynamically (files therein
  ;; won't be found with io/resource). It is simpler to add
  ;; it to an existing source path and remove afterwards.
  (let [runner (io/file source-path
                        "clj_chrome_devtools_runner.cljs")]
    (spit runner (test-runner-forms namespaces))
    (try
      (fun)
      (finally
        (io/delete-file runner)))))


(defn build-clj-chrome [test-runner-namespaces  source-paths test-path]
  (with-test-runner-source test-runner-namespaces test-path
    #(cljs/build (cljs/inputs "src/cljs" "test/cljs");(conj source-paths test-path) ;"src/cljs" "test/cljs")
                 {:output-dir           "resources/public/js"
                  :output-to            "resources/public/js/test_keystore.js"
                  :main                 "clj-chrome-devtools-runner"
                  :asset-path           "resources/public/js"
                  :optimizations :none
                  :source-map true
                  :source-map-timestamp true
                  :cache-analysis true
                  :warnings {:single-segment-namespace false}}))
  {:js "resources/public/js/test_keystore.js"
   :js-directory "resources/public/js"})


(defn- test-page [js]
  (str "<html>"
       "  <head>"
       "  </head>"
       "  <body onload=\"clj_chrome_devtools_runner.run_chrome_tests();\">"
       "    <script type=\"text/javascript\" src=\"" js "\">"
       "    </script>"
       "  </body>"
       "</html>"))


(defn- file-handler [{:keys [uri request-method]}]
  (let [file (io/file "." (subs uri 1))]
    (if (and (= request-method :get) (.canRead file))
      (do
        {:status 200
         :headers {"Content-Type" (cond
                                    (str/ends-with? uri ".html")
                                    "text/html"

                                    (str/ends-with? uri ".js")
                                    "application/javascript"

                                    :default
                                    "application/octet-stream")}
         :body (slurp file)})

      {:status 404})))


(def ^{:doc "cljs.test failure/error report regex"
       :private true}
  final-test-report-pattern #"(\d+) failures, (\d+) errors.")

(defn- assert-test-result [msg]
  (let [[match errors failures] (re-matches final-test-report-pattern msg)]
    (if match
      (if-not  (= "0" errors failures)
        [false "ClojureScript tests had failures or errors, see previous output for details."]
        [true "All tests ok"])
      nil)))

(defn- read-console-log-messages []
  (loop []
    (let [msgs (automation/evaluate "clj_chrome_devtools_runner.get_printed()")]
      (doseq [m (mapcat #(str/split % #"\n") msgs)]
        (println "[CLJS] " m))
      (if-let [res (some #(if-not (nil? %) %) (for [msg msgs]
                                                (assert-test-result msg)))]
           res
          (do
            (Thread/sleep 100)
            (recur))))))

(def test-ok (atom false))

(defn run-test-cases
  ([build-output]
   (run-tests build-output nil))
  ([{:keys [js]} {:keys [no-sandbox?]}]
   (let [chrome-fixture (create-chrome-fixture {:headless? true :no-sandbox? no-sandbox?})
         f (File/createTempFile "test" ".html"
                                (io/file "."))]
     (spit f (test-page js))
     ;check that the test page is readable - force the write to complete
     (print "\n Temp html file " (if (.isFile f) "was created" "was not created"))
     (print "\n Temp html file " (if (.canRead f) "can be read\n" "could not be read\n"))
     (flush)
     (chrome-fixture
       (fn []
         (let [port (random-free-port)
               server (http-server/run-server file-handler {:port port})]
           (try
             (automation/to (str "http://localhost:" port "/" (.getName f)))
             (reset! test-ok (first (read-console-log-messages)))
             (print "\nTest was " (if @test-ok "GOOD\n" "BAD\n"))
             (print "\nSHUTTING THE SERVER DOWN\n")
             (server)
             (finally
               (io/delete-file f)))))))))


(defn -main [& args]
   (deps/resolve-deps (merge req-deps common-deps) nil)
   (case (first args)
     ":prod-build" (do
                     (prod-build)
                     (System/exit 0))
     ":demo-build" (do
                     (demo-build)
                     (System/exit 0))
     ":test"  (let [built (build-clj-chrome ["sa.sa-store.test-keystore"] ["src/cljs" "src/cljc"] "test/cljs")]
                 (print "\nRUN TESTS ")
                 (run-test-cases built {:no-sandbox? true})
                 (flush)
                 (if @test-ok
                   (System/exit 0)
                   (System/exit 1)))
     (print "\n\n either :demo-build :test or :prod-build for prod build \n\n")))
