(ns sa.sa-store.git-url
  "
    Gets a model by github / bit bucket raw url.  No other operations supported.
  "
  (:require [ajax.core         :refer [GET]]
            [sa.sa-store.store :refer [SaStore]]
            [ajax.protocols    :refer [-body]]
            [cljs.core.async   :refer [>! go-loop alts!] :refer-macros [go]]
            [cognitect.transit :as transit]
            [cljs.reader       :as edn]
            [taoensso.timbre   :as timbre
                               :refer-macros [log  trace info error]]))



(defn read-resp [xhrio]
   (-> xhrio -body edn/read-string))

(defn resp-handler  [chan resp]
  (info "git raw url get model")

  (if resp

    (go
      (let [r (transit/reader :json)
            res  (try
                      (transit/read r resp)

                    (catch js/Error e
                        (println (str e))
                        ::not-found))]
         (info "git raw url result : res")
         (if (not= res nil)
            (go (>! chan res))
            (go (>! chan ::not-found)))


       (go (>! chan resp))))
    (go (>! chan ::no-resp))))

(defn error-handler [resp](println "error : " resp))

(deftype GitURLStore []
    SaStore

    (list-model-names [this c]
      (go (>! c :not-implemented)))

    (update-model [this c id model]
      (go (>! c :not-implemented)))

    (get-model [this c name]
      (go (>! c :not-implemented)))

    (get-model-by-uri [this chan uri]
      (GET uri   {:handler (partial resp-handler chan)
                  :error-handler error-handler}))


    (delete-model [this c name]
      (go (>! c :not-implemented)))

    (delete-all [this c]
      (go (>! c :not-implemented)))

    (delete-model-by-uri [this c url]
      (go (>! c :not-implemented)))

    (summarise-all [this c]
      (go (>! c :not-implemented)))

    (update-summary [this c uuid summary]
      (go (>! c :not-implemented))))
