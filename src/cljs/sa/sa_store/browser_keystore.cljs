(ns sa.sa-store.browser-keystore
  "A SaStore type that operates on the browser key store."
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.reader       :as reader]
            [sa.sa-store.store :refer [SaStore]]
            [clojure.string    :as string]
            [cognitect.transit :as transit]
            [cljs.reader       :as rdr]
            [clojure.walk      :as wk]
            [cljs.core.async   :refer [put! chan <! >! timeout close! alts!]]
            [taoensso.timbre   :as timbre
                               :refer-macros [log  trace info error]]))



(def model-prefix "sbnl.sa")

(def READ-TIMEOUT 2000)


(defn is-sa-model? [name]
  (string/starts-with? name model-prefix))

(defn are-models? [names]
  (filter is-sa-model? names))



(defn make-model-name
  "Given a name like 22 return a key like sbnl.sa.22"
  [name]
  (str model-prefix "." name))

(defn get-model-name
  "Given the key name extract the name part eg
   sbnl.sa.22 -> 22"
  [key-name]
  (last (string/split key-name #"\.")))


;Note wk/stringify-keys doesnt preserve namespaces (uses name).
(defn layout-keys-to-string [layout]
  (into {} (map (fn [[k v]] (let [[sns sk] [(namespace k) (name k)]]
                              {(str sns "/" sk) v})) layout)))



(defn layout-keys-from-string [layout]
  (wk/keywordize-keys layout))

(defn model-layout-keys-to-str [m]
  (if-let [l (:layout m)]
    (->> l
         layout-keys-to-string
         (assoc-in m [:layout]))
    m))

(defn model-layout-keys-from-str [sm]
  (if-let [l (:layout sm)]
    (->> l
        layout-keys-from-string
        (assoc-in sm [:layout]))
    sm))

(defn make-summaries
  ""
  []
  (let [keys (.keys js/Object (.-localStorage js/window))
        r (transit/reader :json)]
      (for [key (are-models? keys)]
          (let [model
                  (try
                      ;(rdr/read-string)
                      (transit/read r
                        (.getItem (.-localStorage js/window) key))
                    (catch js/Error e
                      (println (str e))
                      ::not-found))
                sum (assoc (:sadf/meta model) :sadf/uri key)]
            sum))))



(defn update-summary-handler
  [uuid summary model]
  (.setItem (.-localStorage js/window) (str model-prefix "." name) (assoc model :sadf/meta summary))
  {(keyword name) model})


(deftype KeyStore []
    SaStore
    (list-model-names [this chan]
         (info "keystore list model names")
         (go (>! chan (map get-model-name (are-models? (js->clj (.keys js/Object (.-localStorage js/window))))))))

    (update-model [this chan name model]
        (info "keystore update model : " name)
        (let [w (transit/writer :json)
              r (transit/reader :json)]
          (go (>! chan  (try
                          (do
                            (.setItem (.-localStorage js/window) (make-model-name name) (transit/write w model))
                            {(keyword name) (transit/read r (.getItem (.-localStorage js/window) (make-model-name name)))})
                            ; (.setItem (.-localStorage js/window) (make-model-name name) (pr-str (model-layout-keys-to-str model)))
                            ; {(keyword name)  (model-layout-keys-from-str (rdr/read-string  (.getItem (.-localStorage js/window) (make-model-name name))))})
                         (catch js/Error e
                           (error e)
                           (println (str e))
                           ::update-failed))))))
;(reader/read-string
    ;(update-model [this chan name model])

    (get-model [this chan name]
        (info "keystore get model : " name)
        (go
          (let [r (transit/reader :json)
                res  (try
                          (transit/read r
;                          (model-layout-keys-from-str
;                           (rdr/read-string
                            (.getItem (.-localStorage js/window) (make-model-name name)))
                        (catch js/Error e
                            (println (str e))
                            ::not-found))]
             (if (not= res nil)
                (go (>! chan res))
                (go (>! chan ::not-found))))))

    (delete-model [this chan name]
        (info "keystore delete model :" name)
        (go (>! chan
                (do (.removeItem (.-localStorage js/window) (make-model-name name))
                  true))))

    (get-model-by-uri [this chan url]
      :not-found)

    (delete-all [this chan]
      :not-found)

    (summarise-all [this chan]
       (info "summarise all models")
       (go (>! chan (make-summaries))))

    (update-summary [this chan name summary]
        (info "Updating summary : " name " with summary : " summary " and name  " (make-model-name name))
        (let [w (transit/writer :json)
              r (transit/reader :json)]
          (go (>! chan
                    (if-let [model (try
                                       (transit/read r
                                     ; (model-layout-keys-from-str
                                     ;   (rdr/read-string
                                         (.getItem (.-localStorage js/window) (make-model-name name)))
                                     (catch js/Error e ::not-found))]
                      (if  (= model ::not-found)
                        ::not-found
                        (do
                          (.setItem
                             (.-localStorage js/window)
                             (make-model-name name)
                             (transit/write w (assoc model :sadf/meta summary)))
                          {(keyword name) (assoc model :sadf/meta summary)}))
                      ::not-found))))))
