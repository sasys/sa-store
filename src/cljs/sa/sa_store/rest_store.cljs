(ns sa.sa-store.rest-store
  (:require [ajax.core         :refer [GET POST PUT DELETE]]
            [ajax.edn          :refer [edn-response-format edn-request-format]]
            [ajax.transit      :refer [transit-request-format transit-response-format]]
            [sa.sa-store.store :refer [SaStore]]
            [cognitect.transit :as transit]
            [cljs.reader       :as edn]
            [cljs.core.async   :as async :refer [put! chan <! >! close!]]
            [ajax.protocols    :refer [-body]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def edn-content-type "application/edn")
(def transit-json-content-type "application/transit+json")

(def content-type transit-json-content-type)

;Fall back values for the rest data store service
(def DATA_SERVICE_HOST "store.sa.softwarebynumbers.com")
(def DATA_SERVICE_PORT 80)

(def model-prefix DATA_SERVICE_HOST)

(def path (str "http://" DATA_SERVICE_HOST))

(def url "api/model")

(def get-models (str path ":" DATA_SERVICE_PORT "/" url))
(def post-model (str path ":" DATA_SERVICE_PORT "/" url))
(def put-model  (str path":" DATA_SERVICE_PORT "/" url))
(def get-model-summaries (str path ":" DATA_SERVICE_PORT "/" url "/summary"))
(def put-summary (str path ":" DATA_SERVICE_PORT "/" url "/summary"))
(def delete (str path ":" DATA_SERVICE_PORT "/" url))


(defn read-resp [xhrio]
   (-> xhrio -body edn/read-string))

(defn resp-handler
  ([chan resp]
   (resp-handler chan ">>>>> rest api response " resp))
  ([chan msg resp]
   (if resp
    (go (>! chan (js->clj resp)))
    (go (>! chan ::no-resp)))))





;(defn error-handler [resp](println "error : " resp))


(defn find-model
  "puts on the channel the model if it exists or false"
  [chan id]
  (let [r (transit/reader :json-verbose)
        handler (fn [resp] (println "find model handler with resp : " resp)(go (>! chan resp)))  ;the accept type is transit, we dont need a reader here
        error-handler (fn [r] (println "find model error handler with resp : " r)(go (>! chan false)))]
    (GET (str get-models "/" id) {:headers {"Accept" "application/transit+json"}
                                  ; :response-format {:read read-resp
                                  ;                   :description "edn"
                                  ;                   :content-type [content-type]}
                                  :format :transit
                                  ;:response-format (transit-response-format)
                                  :handler handler
                                  :error-handler error-handler})))


(defn new-model
  "Makes a new model returning the model or false"
  [chan model]
  (print "NEW NEW NEW  MODEL HERE : " model)
  (let [handler  #(go (>! chan %))
        error-handler (fn [r] (println "new model error handler with resp : " r)(go (>! chan false)))
        w (transit/writer :json-verbose)]
    (POST post-model  {:body (transit/write w model)
                       :headers {"Content-Type" content-type}
                       ; :response-format {:read read-resp
                       ;                   :description "edn"
                       ;                   :content-type [content-type]}
                       :timeout 1500
                       :format :transit
                       :response-format (transit-response-format)
                       ;:response-format :transit
                       :handler handler
                       :error-handler error-handler})))


(defn update-model
  "Updates an existing model.
   The model must exist.
   The model must have its uuid set.
   Writes the updated model to the channel or false."
  [chan id model]
  (print "UPDATE MODEL HERE : " model)
  (let [handler #(go (>! chan %))
        error-handler (fn [r] (println "update-model error handler with resp : " r)(go (>! chan false)))
        w (transit/writer :json-verbose)]
      (PUT (str put-model "/" id)  {:body (transit/write w model)
                                    :headers {"Content-Type" content-type}
                                    ; :response-format {:read read-resp
                                    ;                   :description "edn"
                                    ;                   :content-type [content-type]}
                                    :timeout 1500
                                    :format :transit
                                    :response-format (transit-response-format)
                                    :handler handler
                                    :error-handler error-handler})))


(deftype RestStore []
    SaStore
    (list-model-names [this chan]  (GET get-models  {;:headers {"Content-Type" content-type}
                                                     ;:format :transit
                                                     ;:response-format {:read read-resp
                                                    ;                   :description "edn"
                                                    ;                   :content-type [content-type]
                                                     :handler (partial resp-handler chan)
                                                     :error-handler (fn [resp] (println "list-model-names error : " resp) (go (>! chan false)))}))
    ;If the model does not exist with the given id, make a new one with a post
    ;otherwise put an update.
    (update-model
      [this c id model]
      (let [exists-chan (chan)]
        (find-model exists-chan id)
        (go (if (= false (<! exists-chan))
              (new-model c model)
              (update-model c id model)))))




    (get-model [this chan name] (GET (str get-models "/" name) {;:headers {"Content-Type" content-type}
                                                                :format :transit
                                                                ;:response-format {:read read-resp
                                                                ;                  :description "edn"
                                                                ;                  :content-type [content-type]
                                                                :timeout 1500
                                                                :response-format (transit-response-format)
                                                                :handler (partial resp-handler chan)
                                                                :error-handler (fn [resp] (println "get-model error : " resp) (go (>! chan false)))}))

    (get-model-by-uri [this chan uri] (GET uri {:format :transit
                                                :response-format (transit-response-format)
                                                :handler (partial resp-handler chan)
                                                :error-handler (fn [resp] (println "get-model-by-uri error : " resp) (go (>! chan false)))}))


    (delete-model [this chan name] (DELETE (str delete "/" name) {:response-format (transit-response-format)
                                                                  :handler (partial resp-handler chan (str "delete resp for " name))
                                                                  :error-handler (fn [resp] (println "delete-model error : " resp) (go (>! chan false)))}))

    (delete-all [this chan]
      :not-found)

    (delete-model-by-uri [this chan url] (DELETE  url {:headers {"Content-Type" content-type}
                                                       :format :raw
                                                       :response-format {:read read-resp
                                                                         :description "edn"
                                                                         :content-type [content-type]}
                                                       :timeout 1500
                                                       :handler (partial resp-handler chan)
                                                       :error-handler (fn [resp] (println "delete-model-by-uri error : " resp) (go (>! chan false)))}))

    (summarise-all [this chan] (GET get-model-summaries {:response-format (edn-response-format)
                                                         ;:format :edn
                                                         :handler (partial resp-handler chan)
                                                         :error-handler (fn [resp] (println "summarise-all  error : " resp) (go (>! chan false)))}))

    (update-summary [this chan uuid summary] (PUT (str put-summary "/" uuid) {:body  summary
                                                                              :headers { "Content-Type" "application/edn"}
                                                                              :response-format {:read read-resp
                                                                                                :description "edn"
                                                                                                :content-type [content-type]}
                                                                              :format :edn
                                                                              :timeout 1500
                                                                              :handler (partial resp-handler chan)
                                                                              :error-handler (fn [resp] (println "update-summary error : " resp) (go (>! chan false)))})))
