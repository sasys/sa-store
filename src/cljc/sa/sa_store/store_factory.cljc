(ns sa.sa-store.store-factory
   "Store factory makes stores on demand based on the store type hierarchy."
   #?(:clj  (:require [sa.sa-store.in-memory        :refer :all]
                      [sa.sa-store.file-based       :refer :all]
                      [sa.sa-store.couch-based      :refer :all])
      :cljs (:require [sa.sa-store.browser-keystore :refer [KeyStore]]
                      [sa.sa-store.rest-store       :refer [RestStore]]
                      [sa.sa-store.git-url          :refer [GitURLStore]])))




;A hierarchy of storage types.
(def store-hierarchy (-> (make-hierarchy)
                         #?(:clj  (derive ::memory-store ::store))    ;in memory store for testing
                         #?(:clj  (derive ::file-store   ::store))    ;a file system based store
                         #?(:clj  (derive ::couch-store  ::store))    ;a store based on couch db
                         #?(:cljs (derive ::rest-store   ::store))    ;store to a rest service
                         #?(:cljs (derive ::key-store    ::store))    ;browser key value store
                         #?(:cljs (derive ::git-url      ::store))))  ;This one is a read only by raw url to a model in a public git cloud accouunt



(defmulti get-store (fn [mode] mode) :hierarchy #'store-hierarchy)

#?(:clj  (defmethod get-store ::memory-store [mode]
          sa.sa-store.in-memory/memory-store))

#?(:clj  (defmethod get-store ::file-store [mode]
          sa.sa-store.file-based/file-store))

#?(:clj  (defmethod get-store ::couch-store [mode]
          sa.sa-store.couch-based/couch-store))

#?(:cljs (defmethod get-store ::key-store [mode]
          (KeyStore.)))

#?(:cljs (defmethod get-store ::rest-store [mode]
          (RestStore.)))

#?(:cljs (defmethod get-store ::git-url [mode]
          (GitURLStore.)))
