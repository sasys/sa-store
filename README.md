
[![CircleCI](https://circleci.com/bb/sbnlocean/sa-store.svg?style=svg)](https://circleci.com/bb/sasys/sa-store)
[![codecov](https://codecov.io/bb/sasys/sa-store/branch/master/graph/badge.svg)](https://codecov.io/bb/sasys/sa-store)  (Coverage for couch and file based only)
# sa-store

Component is a store abstraction for the sa project.

Created as a cljc project to be deployed to the client and/or the server
as required.

Browser code supports:

* Rest service store - see below.
* browser key store

Server side supports (via a REST API):
* CouchDB
* File filesystem
* Git store under development




## For CouchDB Support Requires ENV VARS  

    DB_HOST - Host name of couch db       - defaults to localhost
    DB_PORT - Port number of the couch db - defaults to 5984

And in the short term (subject to change):

    DB_NAME - Name of the data base       - defaults to sa


## TODO

* [x]  Fix up the project.clj, we dont need the deps now the deps.edn is in place.
* [x] Add a CouchDB backend to this
* []  Move the model name prefix (for local browser store) and the other constants
      into one file because the browser store key is not correct.
* [x] Replace the repeated code for the file store operation dispatch with a macro
* [x] The summarise all still uses (cljs) a handler instead of a chan and is
      not processed by a reader macro
* [x] Create integration tests for the rest store
* [x] list-model-names is not properly implemented in the REST impl
* []  Path and port need to swapped out to external configuration
* [x] The circle ci build is no longer working because of the dep on the os lib
* []  Move the test cases to a cljc and add reader conds and tests for cljs
* [x] Get working on an aws server.
* [x] Implement a file based store
* [x] Modified to use cljc and reader conditionals
* [x] Get integration tests working.
* [x] Currently clj needs to be cljc.


## File Store Operation Dispatch

Replaced the repeated code pattern :
```
(let [out-chan (chan)]
          (go (>! op-chan [list-model-names out-chan])) (<!! out-chan)))
```
With macro and usage like :

```
(defmacro serialise-access
  "
   Use this macro to serialise access to the file based operations.
   f  function to invoke
   o  output channel to put the result on
   ps optional parameters to the function
  "
  [f o & ps]
  `(let [r# (chan)]
    (go (>! ~o [~f r# ~@ps]))
    (<!! r#)))
```

```
(serialise-access list-model-names op-chan))
```


## Usage

```
  lein clean
  clojure -A:depstar
  lein install to build and install in local repo

```



Run clj tests using
```
  clojure -A:test

```

To test the browser storage code
```
  clojure -m sa.loader :test
```

For clj coverage report:

```
  clojure -A:coverage =>

  For example

  |-------------------------+---------+---------|
  |               Namespace | % Forms | % Lines |
  |-------------------------+---------+---------|
  | sa.sa-store.couch-based |   85.80 |   91.80 |
  |  sa.sa-store.file-based |   79.31 |   90.57 |
  |-------------------------+---------+---------|
  |               ALL FILES |   80.48 |   91.02 |
  |-------------------------+---------+---------|


```

Local CircleCI

It is possible to debug the circleci locally, for example :
```
  circleci local execute  --job test-server-stores
```

## License

Copyright © 2017 Software By Numbers Ltd

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
