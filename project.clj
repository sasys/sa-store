(defproject com.sbnl.sa.sa-store "0.1.1-SNAPSHOT"

  :description "A store mechanism that should serve the js and the jvm sides of
                the tool."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure                  "1.10.0"]
                 [org.clojure/clojurescript            "1.10.439"]
                 [cljs-ajax                            "0.8.0"]
                 [com.sbnl.util.os-details             "0.1.0-SNAPSHOT"]
                 [org.clojure/core.async               "0.3.442"]
                 [pjstadig/humane-test-output          "0.8.1"]
                 [com.taoensso/timbre                  "4.10.0"]
                 [com.cognitect/transit-cljs           "0.8.256"]
                 [com.bhauman/cljs-test-display        "0.1.1"]
                 [com.ashafa/clutch                    "0.4.0"]
                 [environ                              "1.1.0"]]

  :plugins [[lein-expectations "0.0.8"]
            [lein-autoexpect   "1.9.0"]]

  :profiles {
             ;lein with-profile devcards figwheel
             :devcards
              {:dependencies [[re-frisk "0.5.4"]
                              [org.clojure/test.check "0.9.0"]
                              [org.clojure/core.async "0.4.490"]]
               :plugins      [[lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]
                              [lein-ring      "0.12.4"]
                              [lein-figwheel  "0.5.17"]]
               :test-paths   ["test/cljs"]
               :cljsbuild
                 {:builds
                   [{:id "devcards"
                     :source-paths ["src/cljs" "test/cljs"]
                     :compiler
                        {:main                 "sa.sa-store.test-keystore"
                         :asset-path           "js/devcards_out"
                         :output-to            "resources/public/js/test_store_devcards.js"
                         :output-dir           "resources/public/js/devcards_out"
                         :source-map-timestamp true
                         :source-map           true
                         :optimizations :none
                         :pretty-print true}
                     :figwheel {:on-jsload "sa.sa-store.test-keystore/init"}}]}}}

  :figwheel {:server-port 3450
             :repl        true
             :css-dirs ["resources/public/css"]}

  :source-paths ["src/clj" "src/cljc" "src/cljs"]
  :test-paths   ["test/clj"]

  :clean-targets ^{:protect false} ["resources/public/js"
                                    "target"])
